import { NgModule } from '@angular/core';

import {
  MatInputModule,
  MatCardModule,
  MatButtonModule,
  MatListModule,
  MatIconModule,
  MatToolbarModule,
  MatSelectModule,
  MatSnackBarModule,
  MatMenuModule,
  MatCheckboxModule,
  MatDialogModule,
  MatFormFieldModule,
  MatProgressBarModule,
  MatRippleModule,
  MatTooltipModule,
  MatStepperModule,
  MatNativeDateModule,
  MatAutocompleteModule,
  MatChipsModule
} from '@angular/material';

const MODULES = [
  MatInputModule,
  MatCardModule,
  MatButtonModule,
  MatListModule,
  MatIconModule,
  MatToolbarModule,
  MatSelectModule,
  MatSnackBarModule,
  MatMenuModule,
  MatCheckboxModule,
  MatDialogModule,
  MatFormFieldModule,
  MatProgressBarModule,
  MatMenuModule,
  MatRippleModule,
  MatTooltipModule,
  MatStepperModule,
  MatAutocompleteModule,
  MatChipsModule
];

@NgModule({
  imports: MODULES,
  exports: [...MODULES, MatNativeDateModule],
})

export class MaterialModule { }
