import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { MatomoInjector } from 'ngx-matomo';
import { Router, Event, NavigationStart, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import { filter } from 'rxjs/operators';
import { TrackingService } from '@app/services/tracking.service';
import { environment } from '@app/../environments/environment';

@Component({
  selector: 'c2g-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'cool-to-go';
  constructor(
    private matIconRegistry: MatIconRegistry,
    private sanitizer: DomSanitizer,
    private matomoInjector: MatomoInjector,
    private router: Router,
    private location: Location,
    private tracker: TrackingService
  ) {
    if (environment.production) {
      this.matomoInjector.init('https://matomo.turbine.coop/', 3);
      this.router.events.pipe(
        filter((event: Event) => event instanceof NavigationStart || event instanceof NavigationEnd)
      ).subscribe((event: NavigationStart | NavigationEnd) => {
        if (event instanceof NavigationStart) {
          this.tracker.prepareTracking(this.location.path());
        } else if (event instanceof NavigationEnd) {
          this.tracker.processRoute(this.location.path());
        }
      });
    }
    this.matIconRegistry.addSvgIcon('_elderly',
      this.sanitizer.bypassSecurityTrustResourceUrl('../assets/images/elderly.svg')
    );
    this.matIconRegistry.addSvgIcon('_jump',
      this.sanitizer.bypassSecurityTrustResourceUrl('../assets/images/jump.svg')
    );
    this.matIconRegistry.addSvgIcon('_people',
      this.sanitizer.bypassSecurityTrustResourceUrl('../assets/images/people.svg')
    );
    this.matIconRegistry.addSvgIcon('_caring',
      this.sanitizer.bypassSecurityTrustResourceUrl('../assets/images/caring.svg')
    );
    this.matIconRegistry.addSvgIcon('_wheelchair',
      this.sanitizer.bypassSecurityTrustResourceUrl('../assets/images/wheelchair.svg')
    );
    this.matIconRegistry.addSvgIcon('_child',
      this.sanitizer.bypassSecurityTrustResourceUrl('../assets/images/child.svg')
    );
    this.matIconRegistry.addSvgIcon('nature_walk',
      this.sanitizer.bypassSecurityTrustResourceUrl('../assets/images/nature_walk.svg')
    );
    this.matIconRegistry.addSvgIcon('nature_walk-light',
      this.sanitizer.bypassSecurityTrustResourceUrl('../assets/images/nature_walk-light.svg')
    );
    this.matIconRegistry.addSvgIcon('nature_walk-accent',
      this.sanitizer.bypassSecurityTrustResourceUrl('../assets/images/nature_walk-accent.svg')
    );
  }
}