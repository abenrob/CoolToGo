import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from '@app/containers/404/not-found.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: './children/landing/landing.module#LandingPageModule'
  },
  {
    path: 'profil',
    loadChildren: './children/profile/profile.module#ProfilePageModule'
  },
  {
    path: 'lieux',
    loadChildren: './children/locations/locations.module#LocationPageModule'
  },
  {
    path: 'apropos',
    loadChildren: './children/info/info.module#InfoPageModule'
  },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
