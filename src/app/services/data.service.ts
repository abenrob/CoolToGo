import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
    ProfileItem,
    FullUserProfile,
    modesOfTransport
} from '@app/services/data.service.sources';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { schemaVersion } from '@app/services/data.service.sources';

@Injectable()
export class DataService {
    fullUser: Observable<FullUserProfile>;
    private FULLUSER: BehaviorSubject <FullUserProfile>;
    protected profiles: ProfileItem[];
    protected categories: ProfileItem[];

    constructor(
        public http: HttpClient
    ) {}

    async load(): Promise<void> {
        let user: FullUserProfile = JSON.parse(localStorage.getItem('user'));
        let initialUser: FullUserProfile = null;
        if (user && user.schemaVersion === schemaVersion) {
            this.profiles = user.userProfiles
            this.categories = user.activityFilters
        } else {
            if (user) {
                localStorage.removeItem('user');
            }

            this.profiles = await this.http.get('./assets/json/profiles.json').pipe(map((data: ProfileItem[]) => data)).toPromise();
            this.categories = await this.http.get('./assets/json/categories.json').pipe(map((data: ProfileItem[]) => data)).toPromise();
            const modeOfTransport = modesOfTransport.find(mode => mode.selected);

            initialUser = {
                userProfiles: this.profiles,
                transportModes: modesOfTransport,
                activityFilters: this.categories,
                transport: modeOfTransport,
                location: null,
                maxTime: 1800,
                schemaVersion
            };
        }

        if (initialUser) {
            user = initialUser;
        }

        this.FULLUSER = new BehaviorSubject(user) as BehaviorSubject<FullUserProfile>;
        this.fullUser = this.FULLUSER.asObservable();
        this.fullUser.subscribe(user => {
            localStorage.setItem('user', JSON.stringify(user));
        });

        this.updateUser(user);
    }

    updateUser(updatedUser: any) {
        this.FULLUSER.next(Object.assign(
            JSON.parse(localStorage.getItem('user')),
            updatedUser
        ));
    }

    setProfileFilters(profiles: ProfileItem[]) {
        const curUser: FullUserProfile = JSON.parse(localStorage.getItem('user'));
        this.FULLUSER.next(Object.assign(
            curUser,
            {
                userProfiles: profiles
            }
        ));
    }

    setMode(mode: ProfileItem) {
        const curUser: FullUserProfile = JSON.parse(localStorage.getItem('user'));
        this.FULLUSER.next(Object.assign(
            curUser,
            {
                transport: mode,
                transportModes: curUser.transportModes.map(transportMode => {
                    transportMode.selected = transportMode.value === mode.value;
                    return transportMode;
                })
            }
        ));
    }

    setActivityFilters(filters: ProfileItem[]) {
        const curUser: FullUserProfile = JSON.parse(localStorage.getItem('user'));
        this.FULLUSER.next(Object.assign(
            curUser,
            {
                activityFilters: filters
            }
        ));
    }

    getCategory(value: number) {
        return this.categories.find(filter => filter.value === value);
    }

    getCategories(values: number[]) {
        return this.categories.filter(filter => values.includes(+filter.value));
    }

    getProfile(value: number) {
        return this.profiles.find(filter => filter.value === value);
    }

    getProfiles(values: number[]) {
        return this.profiles.filter(filter => values.includes(+filter.value));
    }

    setTime(maxTime: number) {
        const curUser: FullUserProfile = JSON.parse(localStorage.getItem('user'));
        this.FULLUSER.next(Object.assign(
            curUser,
            {
                maxTime
            }
        ));
    }
}
