import { HttpClient } from '@angular/common/http';
import { Observable, of as ObservableOf } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Point, FeatureCollection, MultiPolygon, featureCollection, difference, bboxPolygon } from '@turf/turf';
import { TargomoClient, LatLngId, LatLngIdTravelTime, TimeRequestOptions, TravelType } from '@targomo/core';
import { APP_DI_CONFIG } from '@app/app-config.module';
import { FullUserProfile } from './data.service.sources';
import * as moment from 'moment';

@Injectable()
export class ReachabilityService {
    public client = new TargomoClient('westcentraleurope', APP_DI_CONFIG.tgmKey);
    protected locations: FeatureCollection<Point>;
    protected locationsByCategory: any = {};

    constructor(
        public http: HttpClient
    ) { }

    public getJSON(): Observable<FeatureCollection<Point>> {
        const jsonURL = './assets/json/locations.json';
        if (this.locations && this.locations.features && this.locations.features.length > 0) {
            return ObservableOf(this.locations);
        } else {
            return this.http.get(jsonURL).pipe(
                map((jsonData: any) => {
                    this.locations = jsonData.data;
                    return this.locations;
                })
            );
        }
    }

    public getJSONByCategory(category: string | number): Observable<FeatureCollection<Point>> {
        const jsonURL = `./assets/json/locations-${category}.json`;
        const refLocs = this.locationsByCategory[category];
        if (refLocs && refLocs.features && refLocs.features.length > 0) {
            return ObservableOf(refLocs);
        } else {
            return this.http.get(jsonURL).pipe(
                map((jsonData: any) => {
                    this.locationsByCategory[category] = jsonData.data;
                    return this.locationsByCategory[category];
                })
            );
        }
    }

    public async getSingleLocation(id: number): Promise<any> {
        const location = await this.getJSON().toPromise().then(locations => {
            const match = locations.features.find(feature => {
                return +feature.properties.id === +id;
            });
            return match ? match.properties : null;
        });

        return Object.assign({}, location);
    }

    public async routeSingleLocation(id: number, user: FullUserProfile): Promise<any> {
        const source: LatLngId = {
            id: 'user-location',
            lat: (user.location.geometry.coordinates[1] as number),
            lng: (user.location.geometry.coordinates[0] as number)
        };

        const location = await this.getJSON().toPromise().then(locations => {
            const match = locations.features.find(feature => {
                return +feature.properties.id === +id;
            });
            return match ? match.properties : null;
        });

        const target: LatLngId = {
            id: location.id,
            lat: location.latitude as number,
            lng: location.longitude as number,
        };

        const options: any = {
            travelType: user.transport.value as TravelType,
            maxEdgeWeight: 7200, // max allowed for "pro" plan
            edgeWeight: 'time',
            useClientCache: true,
            pathSerializer: 'geojson',
            polygon: {
                srid: 4326
            }
        };

        return this.client.routes.fetch([source], [target], (options as any));
    }

    public async getPolygons(user: FullUserProfile): Promise<FeatureCollection<MultiPolygon>> {
        const source: LatLngId = {
            id: 'user-location',
            lat: (user.location.geometry.coordinates[1] as number),
            lng: (user.location.geometry.coordinates[0] as number)
        };

        const options: any = {
            travelType: user.transport.value as TravelType,
            maxEdgeWeight: user.maxTime,
            edgeWeight: 'time',
            useClientCache: true,
            travelEdgeWeights: [user.maxTime],
            srid: 4326,
            simplify: 50,
            serializer: 'geojson',
            buffer: 0.002
        };

        return this.client.polygons.fetch([source], options).then(results => {
            const inverse = featureCollection([
                difference(
                    bboxPolygon([-5.27, 42.22, 8.44, 51.37]),
                    results.features[0]
                )
            ]);
            return (Object.assign({}, inverse) as FeatureCollection<MultiPolygon>);
        });
    }

    public async filterLocations(user: FullUserProfile): Promise<FeatureCollection<Point>> {
        const source: LatLngId = {
            id: 'user-location',
            lat: (user.location.geometry.coordinates[1] as number),
            lng: (user.location.geometry.coordinates[0] as number)
        };

        const category = user.activityFilters.find(f => f.selected).value;
        const filteredLocations = await this.getJSONByCategory(category).toPromise();

        const targets: LatLngIdTravelTime[] = filteredLocations.features.filter(feature => {
            const start = feature.properties.date_start ? moment(feature.properties.date_start) : null;
            const end = feature.properties.date_end ? moment(feature.properties.date_end) : null;
            const now = moment.now();
            const eventFilter = (() => {
                let isCurrent = true;
                if (feature.properties.type === 'FETE_ET_MANIFESTATION' && start && end) {
                    isCurrent = start.subtract(1, 'day').isBefore(now) && end.add(1, 'day').isAfter(now)
                }
                return isCurrent
            })()
            const profileFilter = feature.properties.profiles.filter(featureProfile => {
                return user.userProfiles
                    .filter(profile => profile.selected)
                    .map(profile => profile.value)
                    .includes(featureProfile);
            }).length > 0;
            return profileFilter
                && eventFilter
        }).map(feature => {
            return {
                id: feature.properties.id,
                lat: feature.geometry.coordinates[1],
                lng: feature.geometry.coordinates[0]
            };
        });


        const options: TimeRequestOptions = {
            travelType: user.transport.value as TravelType,
            maxEdgeWeight: user.maxTime,
            edgeWeight: 'time',
            useClientCache: true
        };

        if (targets.length) {
            return this.client.reachability.locations([source], targets, options).then(results => {
                const timedFeatures = filteredLocations.features.map(feature => {
                    const match = results.find(result => result.id === feature.properties.id);
                    feature.properties.travelTime = match ? match.travelTime : null;
                    feature.properties.mode = user.transport.value;
                    feature.properties.transportIcon = user.transport.icon;
                    return feature;
                }).filter(feature => {
                    return feature.properties.travelTime;
                }).sort((a: any, b: any) => {
                    return a.properties.travelTime > b.properties.travelTime ? 1 : -1;
                });

                return Object.assign({}, filteredLocations, { features: timedFeatures });
            });
        } else {
            return featureCollection([]);
        }

    }
}
