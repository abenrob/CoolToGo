import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { FeatureCollection, Feature, Geometry } from '@turf/turf';

@Injectable()
export class BanService {
    constructor(public http: HttpClient) { }

    reverse(lng: number, lat: number): Observable<Feature<Geometry>[]> {
        const banURL = `https://api-adresse.data.gouv.fr/reverse/?lon=${lng}&lat=${lat}`;
        return new Observable(observer => {
            this.http
                .get(banURL)
                .subscribe((address: FeatureCollection) => {
                    observer.next(address.features as Feature<Geometry>[]);
                    observer.complete();
                });
        });
    }

    search(q: string): Observable<Feature<Geometry>[]> {
        const banURL = `https://api-adresse.data.gouv.fr/search/?lon=5.7245&lat=45.1885&q=${q}`;
        return new Observable(observer => {
            this.http
                .get(banURL)
                .subscribe((address: FeatureCollection) => {
                    observer.next(address.features as Feature<Geometry>[]);
                    observer.complete();
                });
        });
    }
}
