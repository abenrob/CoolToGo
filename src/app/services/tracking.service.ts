import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ReachabilityService } from './reachability.service';
import { MatomoTracker } from 'ngx-matomo';

@Injectable()
export class TrackingService {
    constructor(
        private title: Title,
        private reachabilityService: ReachabilityService,
        private matomoTracker: MatomoTracker
    ) { }

    processRoute(route: string){
        const re = /\/\w+\/\d+/g;
        if (route === '/profil'){
            const title = 'CoolToGo - Définir votre profil';
            this.logView(route, title);
            this.title.setTitle(title);
        } else if (route === '/apropos') {
            const title = 'CoolToGo - À propos du projet';
            this.logView(route, title);
        } else if (route === '/lieux/liste') {
            const title = 'CoolToGo - liste des lieux';
            this.logView(route, title);
        } else if (route === '/lieux/carte') {
            const title = 'CoolToGo - carte des lieux';
            this.logView(route, title);
        } else if (re.test(route)) {
            const id = +route.split('/')[route.split('/').length-1]
            this.reachabilityService.getSingleLocation(id).then(location => {
                console.log(location)
                if (location) {
                    const title = `CoolToGo - ${location.title}`;
                    this.logView(route, title);
                } else {
                    const title = `CoolToGo - Activité pas trouvé`;
                    this.logView(route, title);
                }
            });
        } else if (route === '') {
            const title = `CoolToGo !`;
            this.logView(route, title);
        }
    }

    prepareTracking(route: string){
        this.matomoTracker.setReferrerUrl(route);
    }

    logView(route: string, title: string){
        this.matomoTracker.setCustomUrl(route);
        this.title.setTitle(title);
        this.matomoTracker.setDocumentTitle(title);
        this.matomoTracker.setGenerationTimeMs(0);
        this.matomoTracker.trackPageView();
    }
}
