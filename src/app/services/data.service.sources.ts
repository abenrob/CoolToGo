import { Feature, Point } from '@turf/turf';

export const schemaVersion = '1.0.5';

export interface FullUserProfile {
    userProfiles: ProfileItem[];
    transportModes: ProfileItem[];
    activityFilters: ProfileItem[];
    transport: ProfileItem;
    location: Feature<Point>;
    maxTime: number;
    schemaVersion: string;
}

export interface ProfileItem {
    label: string;
    selected: boolean;
    value: string|number;
    icon: string;
    icon_type: 'svg'|'material';
}

export const modesOfTransport: ProfileItem[] = [
    {
        label: 'Pieton',
        selected: true,
        value: 'walk',
        icon: 'directions_walk',
        icon_type: 'material'
    },
    {
        label: 'Vélo',
        selected: false,
        value: 'bike',
        icon: 'directions_bike',
        icon_type: 'material'
    },
    {
        label: 'Transports en commun',
        selected: false,
        value: 'transit',
        icon: 'directions_transit',
        icon_type: 'material'
    },
    {
        label: 'Voiture',
        selected: false,
        value: 'car',
        icon: 'directions_car',
        icon_type: 'material'
    }
];