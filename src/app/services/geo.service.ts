import { HttpClient } from '@angular/common/http';
import { Observable, of as ObservableOf } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { FeatureCollection, Position as GeoPosition, points, pointsWithinPolygon, Polygon } from '@turf/turf';

@Injectable()
export class GeoService {
    protected metroRegion: FeatureCollection<Polygon>;
    constructor(public http: HttpClient) { }

    public getJSON(): Observable<FeatureCollection<Polygon>> {
        const jsonURL = './assets/json/metro.geojson';
        if (this.metroRegion) {
            return ObservableOf(this.metroRegion);
        } else {
            return this.http.get(jsonURL).pipe(
                map((data: FeatureCollection<Polygon>) => {
                    this.metroRegion = data;
                    return data;
                })
            );
        }
    }

    async inMetro(coordinates: GeoPosition) {
        const location = points([coordinates]);
        const metro: FeatureCollection<Polygon> = await this.getJSON().toPromise();
        const within = pointsWithinPolygon(location, metro);
        return within.features.length > 0;
    }
}
