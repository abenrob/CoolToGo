import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { AppRoutingModule } from '@app/app-routing.module';
import { AppComponent } from '@app/app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentsModule } from '@app/components/components.module';
import { NotFoundComponent } from '@app/containers/404/not-found.component';
import { DataService } from '@app/services/data.service';
import { BanService } from '@app/services/ban.service';
import { GeoService } from '@app/services/geo.service';
import { ReachabilityService } from '@app/services/reachability.service';
import { TrackingService } from '@app/services/tracking.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { C2gLocationDialogComponent } from '@app/components/location-dialog/location-dialog.component';
import { C2gSettingsDialogComponent } from '@app/components/settings-dialog/settings-dialog.component';
import { AppConfigModule } from '@app/app-config.module';
import { MatomoModule } from 'ngx-matomo';

export function dataProviderFactory(provider: DataService) {
  return () => provider.load();
}

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent
  ],
  entryComponents: [
    C2gLocationDialogComponent,
    C2gSettingsDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppConfigModule,
    MatomoModule
  ],
  providers: [
    Title,
    DataService,
    { provide: APP_INITIALIZER, useFactory: dataProviderFactory, deps: [DataService], multi: true },
    BanService,
    GeoService,
    ReachabilityService,
    TrackingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
