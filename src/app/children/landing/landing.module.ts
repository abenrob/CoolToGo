import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LandingPageComponent } from '@app/children/landing/containers/landing-page/landing-page.component';
import { LandingPageRoutingModule } from '@app/children/landing/landing-routing.module';
import { ComponentsModule } from '@app/components/components.module';
import { MaterialModule } from '@app/modules';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    LandingPageRoutingModule,
    ComponentsModule,
    MaterialModule
  ],
  declarations: [
    LandingPageComponent,
  ],
  exports: [LandingPageComponent]
})
export class LandingPageModule {}
