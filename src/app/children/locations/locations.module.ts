import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LocationPageComponent } from '@app/children/locations/containers/location-page/location-page.component';
import { LocationListComponent } from '@app/children/locations/containers/location-list/location-list.component';
import { LocationMapComponent } from '@app/children/locations/containers/location-map/location-map.component';
import { LocationSingleComponent } from '@app/children/locations/containers/location-single/location-single.component';
import { LocationPageRoutingModule } from '@app/children/locations/locations-routing.module';
import { ComponentsModule } from '@app/components/components.module';
import { MaterialModule } from '@app/modules';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    LocationPageRoutingModule,
    ComponentsModule,
    MaterialModule
  ],
  declarations: [
    LocationPageComponent,
    LocationListComponent,
    LocationMapComponent,
    LocationSingleComponent
  ],
  exports: [
    LocationPageComponent,
    LocationListComponent,
    LocationMapComponent,
    LocationSingleComponent
  ]
})
export class LocationPageModule {}
