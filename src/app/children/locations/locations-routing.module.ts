import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LocationListComponent } from '@app/children/locations/containers/location-list/location-list.component';
import { LocationMapComponent } from '@app/children/locations/containers/location-map/location-map.component';
import { LocationSingleComponent } from './containers/location-single/location-single.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'liste'
  },
  {
    path: 'liste',
    component: LocationListComponent
  },
  {
    path: 'carte',
    component: LocationMapComponent
  },
  {
    path: ':id',
    component: LocationSingleComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocationPageRoutingModule {}
