import { Component, OnInit } from '@angular/core';
import { DataService } from '@app/services/data.service';
import { Observable } from 'rxjs';
import { FullUserProfile } from '@app/services/data.service.sources';
import { FeatureCollection, Point, featureCollection } from '@turf/turf';
import { ReachabilityService } from '@app/services/reachability.service';
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'c2g-location-list',
  templateUrl: './location-list.component.html',
  styleUrls: ['./location-list.component.scss']
})
export class LocationListComponent implements OnInit {
  user$: Observable<FullUserProfile>;
  locations: FeatureCollection<Point> = featureCollection([]);
  dataLoaded = false;

  constructor(
    private dataService: DataService,
    private reachabilityService: ReachabilityService,
    private router: Router
  ) {}

  ngOnInit() {
    this.user$ = this.dataService.fullUser;
    this.user$.subscribe(user => {
      if (user.location) {
        this.dataLoaded = false;
        this.reachabilityService.filterLocations(user).then(results => {
          this.locations = results;
          this.dataLoaded = true;
        });
      }
    });
  }

  isEvent(location: any){
    return location.properties.type === 'FETE_ET_MANIFESTATION'
  }

  formatDates(startString: string, endString: string) {
    const start = moment(startString).format('DD/MM/YYYY');
    const end = moment(endString).format('DD/MM/YYYY');
    return start === end ? start : `${start} - ${end}`;
  }

  goToLocation(id: number) {
    this.router.navigate([`/lieux/${id}`]);
  }
}
