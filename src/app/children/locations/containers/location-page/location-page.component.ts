import { Component, OnInit, HostListener } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { C2gLocationDialogComponent } from '@app/components/location-dialog/location-dialog.component';
import { C2gSettingsDialogComponent } from '@app/components/settings-dialog/settings-dialog.component';
import { DataService } from '@app/services/data.service';
import { Observable } from 'rxjs';
import { FullUserProfile, ProfileItem } from '@app/services/data.service.sources';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'c2g-location-page',
  templateUrl: './location-page.component.html',
  styleUrls: ['./location-page.component.scss']
})
export class LocationPageComponent implements OnInit {
  user$: Observable<FullUserProfile>;
  locationsDialogRef: MatDialogRef<C2gLocationDialogComponent, any>;
  settingsDialogRef: MatDialogRef<C2gSettingsDialogComponent, any>;
  public innerWidth: any;
  selectedFilter: ProfileItem;
  filters: ProfileItem[];
  singlePage: boolean;

  constructor(
    public dialog: MatDialog,
    private dataService: DataService,
    private activatedRoute: ActivatedRoute
  ) { }

  openLocationsDialog(): void {
    if (this.locationsDialogRef) { return; }
    this.locationsDialogRef = this.dialog.open(C2gLocationDialogComponent);
  }

  openSettingsDialog(): void {
    this.settingsDialogRef = this.dialog.open(C2gSettingsDialogComponent, {
      panelClass: 'settings-panel',
      width: this.innerWidth < 600 ? '100%' : '400px',
      height: this.innerWidth < 600 ? '100%' : null,
      maxWidth: this.innerWidth < 600 ? '100%' : null,
      maxHeight: this.innerWidth < 600 ? '100%' : null
    });
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      if (params.id){
        this.singlePage = true;
      } else {
        this.singlePage = false;
      }
    })
    this.innerWidth = window.innerWidth;
    this.user$ = this.dataService.fullUser;
    this.user$.subscribe(user => {
      if (!user.location) {
        this.openLocationsDialog();
      }
      this.filters = user.activityFilters;
      this.selectedFilter = user.activityFilters.find(filter => filter.selected);
    });
  }

  setFilter(filter) {
    this.filters.forEach(f => f.selected = f.value === filter.value);
    this.selectedFilter = filter
    this.dataService.setActivityFilters(this.filters);
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.innerWidth = window.innerWidth;
  }

  showFooter() {
    return this.innerWidth <= 600;
  }

  onTabChanged(filter) {
    this.setFilter(filter);
  }

  isSelected(filter) {
    return filter.value === this.selectedFilter.value;
  }
}
