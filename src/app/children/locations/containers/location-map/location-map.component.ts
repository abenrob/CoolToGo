import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { DataService } from '@app/services/data.service';
import { Observable } from 'rxjs';
import { FullUserProfile } from '@app/services/data.service.sources';
import { ReachabilityService } from '@app/services/reachability.service';
import { Point, FeatureCollection, featureCollection, MultiPolygon } from '@turf/turf';
import { C2gListMapComponent } from '@app/components/list-map/map.component';

@Component({
  selector: 'c2g-location-map',
  templateUrl: './location-map.component.html',
  styleUrls: ['./location-map.component.scss']
})
export class LocationMapComponent implements OnInit, AfterViewInit {
  user$: Observable<FullUserProfile>;
  locations: FeatureCollection<Point> = featureCollection([]);
  polygons: FeatureCollection<MultiPolygon> = featureCollection([]);
  isMapReady = false;
  dataFetching = false;
  dataAdded = false;

  @ViewChild('listmap', { static: true }) listmap: C2gListMapComponent;

  constructor(
    private dataService: DataService,
    private reachabilityService: ReachabilityService
  ) { }

  ngOnInit() {
    this.user$ = this.dataService.fullUser;
    this.user$.subscribe(user => {
      if (user.location) {
        Promise.all([
          this.reachabilityService.filterLocations(user),
          this.reachabilityService.getPolygons(user)
        ]).then(([locations, polygons]) => {
          this.locations = locations;
          this.polygons = polygons;
          this.addDataToMap(user);
        });
      }
    });
  }

  ngAfterViewInit() {
    this.listmap.mapReady.subscribe((state) => {
      this.isMapReady = state;
      if (this.isMapReady) {
        this.user$.toPromise().then((user: FullUserProfile) => {
          this.addDataToMap(user);
        });
      }
    });
  }

  addDataToMap(user: FullUserProfile) {
    if (!this.dataFetching) {
      this.dataFetching = true;
      const interval = setInterval(() => {
        this.dataAdded = false;
        if (this.isMapReady && this.locations.features.length > 0 && this.polygons.features.length > 0) {
          this.listmap.populateSources(this.locations, user, this.polygons).then(() => {
            this.dataAdded = true;
          });
          clearInterval(interval);
          this.dataFetching = false;
        } 
      }, 500);
    }
  }
}
