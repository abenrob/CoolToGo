import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { DataService } from '@app/services/data.service';
import { Observable } from 'rxjs';
import { FullUserProfile } from '@app/services/data.service.sources';
import { ReachabilityService } from '@app/services/reachability.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { featureCollection, FeatureCollection, Point, LineString, Feature, point } from '@turf/turf';
import { C2gSingleMapComponent } from '@app/components/single-map/map.component';
import * as moment from 'moment';

@Component({
  selector: 'c2g-location-single',
  templateUrl: './location-single.component.html',
  styleUrls: ['./location-single.component.scss']
})
export class LocationSingleComponent implements OnInit, AfterViewInit {
  user$: Observable<FullUserProfile>;
  id: number;
  location: any = {};
  showDescription = false;
  previousRoute: string;
  locationRoute: FeatureCollection<LineString> = featureCollection([]);
  routeStart: FeatureCollection<Point> = featureCollection([]);
  routeEnd: FeatureCollection<Point> = featureCollection([]);
  travelTime: number = null;
  isMapReady = false;
  dataAdded = false;
  categories: any[] = [];

  @ViewChild('singlemap', { static: true }) singlemap: C2gSingleMapComponent;

  constructor(
    private dataService: DataService,
    private reachabilityService: ReachabilityService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.user$ = this.dataService.fullUser;
    this.activatedRoute.params.subscribe((params: Params) => {
      if (params.id) {
        this.id = params.id;
        this.location = this.reachabilityService.getSingleLocation(this.id).then(location => {
          if (location) {
            this.location = location;
            this.categories = this.dataService.getCategories(this.location.categories);
          } else {
            const notFound = this.snackBar.open('Activité pas trouvé ...', '', {
              duration: 2000, verticalPosition: 'top'
            });
            notFound.afterDismissed().subscribe(() => this.router.navigate(['/lieux/liste']));
          }
        });

        this.user$.subscribe(user => {
          this.reachabilityService.routeSingleLocation(this.id, user).then(async routes => {
            this.routeStart = featureCollection<Point>([user.location]);
            const end = await this.reachabilityService.getSingleLocation(this.id);
            this.routeEnd = featureCollection<Point>([point([end.longitude, end.latitude])]);
            if (routes.length > 0) {
              let travelTime = 0;
              const route = JSON.parse(JSON.stringify(routes[0].features));
              this.locationRoute = featureCollection(route);
              route.forEach((routeFeature: Feature<LineString>) => {
                if (routeFeature.properties.travelTime){
                  travelTime += routeFeature.properties.travelTime;
                }
              });
              this.travelTime = travelTime;
            }

            this.isMapReady = true;
            this.addDataToMap();
          });
        });
      }
    });
  }

  ngAfterViewInit() {
    this.singlemap.mapReady.subscribe(() => {
      this.isMapReady = true;
      this.addDataToMap();
    });
  }

  isEvent(location: any) {
    return location.type === 'FETE_ET_MANIFESTATION'
  }

  formatDate(dateString) {
    return moment(dateString).format('DD/MM/YYYY')
  }
  
  addressFormat(address, code, city) {
    return [address, code, city].filter(e => e != null).join(', ');
  }

  addDataToMap() {
    this.dataAdded = false;
    if (this.isMapReady) {
      this.singlemap.populateSources(this.routeStart, this.routeEnd, this.locationRoute).then(() => {
        this.dataAdded = true;
      });
    }
  }
}
