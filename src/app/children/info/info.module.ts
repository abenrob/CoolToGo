import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { InfoPageComponent } from '@app/children/info/containers/info-page/info-page.component';
import { InfoPageRoutingModule } from '@app/children/info/info-routing.module';
import { ComponentsModule } from '@app/components/components.module';
import { MaterialModule } from '@app/modules';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    InfoPageRoutingModule,
    ComponentsModule,
    MaterialModule
  ],
  declarations: [
    InfoPageComponent,
  ],
  exports: [InfoPageComponent]
})
export class InfoPageModule {}
