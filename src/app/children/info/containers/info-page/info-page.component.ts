import { Component } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'c2g-info-page',
  templateUrl: './info-page.component.html',
  styleUrls: ['./info-page.component.scss']
})
export class InfoPageComponent {
  constructor(
    private LOCATION: Location
  ) {}

  goBack() {
    this.LOCATION.back();
  }
}
