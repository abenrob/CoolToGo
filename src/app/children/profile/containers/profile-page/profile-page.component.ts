import { Component, ElementRef, OnInit, ViewChild, HostListener } from '@angular/core';
import { FullUserProfile, ProfileItem } from '@app/services/data.service.sources';
import { Observable } from 'rxjs';
import { DataService } from '@app/services/data.service';
import { GeoService } from '@app/services/geo.service';
import { Router } from '@angular/router';

@Component({
  selector: 'c2g-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss']
})
export class ProfilePageComponent implements OnInit {
  isMobile: boolean;
  profiles: any[];
  modesOfTransport: any[];
  filters: any[];
  selectedFilter: any;
  selectedProfiles: any[];
  geoLocationEnabled = true;
  isSearchingForCurrentLocation: boolean;
  currentLocationTimeout: any;
  validLocation = false;
  user$: Observable<FullUserProfile>;
  public innerWidth: any;
  validating: boolean = false;

  @ViewChild('geolocator', { static: true }) geolocator: ElementRef;
  constructor(
    private dataService: DataService,
    private geoService: GeoService,
    private router: Router
  ) {
    if (!navigator.geolocation) {
      this.geoLocationEnabled = false;
    }
  }

  ngOnInit() {
    this.innerWidth = window.innerWidth;
    this.user$ = this.dataService.fullUser;
    this.user$.subscribe(user => {
      this.filters = user.activityFilters;
      this.selectedFilter = user.activityFilters.find(filter => filter.selected);
      this.profiles = user.userProfiles;
      this.selectedProfiles = user.userProfiles.filter(filter => filter.selected);
    });
  }

  setProfiles(event, selected) {
    if (event && selected.length === 0) {
      this.profiles.forEach(profile => profile.selected = true);
      this.selectedProfiles = this.profiles.map(profile => profile);
    } else {
      this.profiles.forEach(profile => {
        const match = selected.find(option => option.value.value === profile.value);
        profile.selected = match ? true : false;
      });
    }
    this.dataService.setProfileFilters(this.profiles);
  }

  setFilter(filter) {
    this.filters.forEach(f => f.selected = f.value === filter.value);
    this.selectedFilter = filter
    this.dataService.setActivityFilters(this.filters);
  }

  filterCompare(a: any, b: any) {
    return a.value === b.value;
  }

  setMode(mode: ProfileItem) {
    this.dataService.setMode(mode);
  }

  async validateLocation() {
    this.validating = true;
    this.validLocation = await this.geoService.inMetro((this.geolocator as any).position.geometry.coordinates);
    this.validating = false;
    if (!this.validLocation) {
      (this.geolocator as any).clearInput();
    } else {
      (this.geolocator as any).flagValidLocation();
      this.router.navigate(['/lieux']);
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.innerWidth = window.innerWidth;
  }

  smallScreen() {
    return this.innerWidth <= 600;
  }
}
