import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ProfilePageComponent } from '@app/children/profile/containers/profile-page/profile-page.component';
import { ProfilePageRoutingModule } from '@app/children/profile/profile-routing.module';
import { ComponentsModule } from '@app/components/components.module';
import { MaterialModule } from '@app/modules';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    ProfilePageRoutingModule,
    ComponentsModule,
    MaterialModule
  ],
  declarations: [
    ProfilePageComponent,
  ],
  exports: [ProfilePageComponent]
})
export class ProfilePageModule {}
