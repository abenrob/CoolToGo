import { Component, OnInit } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import { FeatureCollection, Point, LineString, bbox, featureCollection, BBox } from '@turf/turf';
import { Observable, BehaviorSubject } from 'rxjs';
import { APP_DI_CONFIG } from '@app/app-config.module';

@Component({
  selector: 'c2g-single-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class C2gSingleMapComponent implements OnInit {

  /// default settings
  map: mapboxgl.Map;
  style = `https://api.maptiler.com/maps/topo/style.json?key=${APP_DI_CONFIG.maptilerKey}`;
  lat = 45.1885;
  lng = 5.7245;
  mapReady: Observable<boolean>;
  private MAPREADY: BehaviorSubject<boolean>;
  bounds: BBox;

  constructor() {
    this.MAPREADY = new BehaviorSubject(false) as BehaviorSubject<boolean>;
    this.mapReady = this.MAPREADY.asObservable();
  }

  ngOnInit() {
    this.buildMap();
  }

  buildMap() {
    this.map = new mapboxgl.Map({
      container: 'map',
      style: this.style,
      zoom: 14,
      center: [this.lng, this.lat],
      attributionControl: false
    }).addControl(new mapboxgl.NavigationControl()).addControl(new mapboxgl.AttributionControl({
      customAttribution: `<a href='https://targomo.com/developers/resources/attribution/' target='_blank'>&copy; Targomo</a>`,
      compact: false
    }));

    this.map.on('load', () => {
      this.map.resize();
      /// register sources
      this.map.addSource('start', {
        type: 'geojson',
        data: {
          type: 'FeatureCollection',
          features: []
        }
      });

      this.map.addSource('end', {
        type: 'geojson',
        data: {
          type: 'FeatureCollection',
          features: []
        }
      });

      this.map.addSource('lines', {
        type: 'geojson',
        data: {
          type: 'FeatureCollection',
          features: []
        }
      });

      /// create map layers
      this.map.addLayer({
        id: 'lines-walk',
        source: 'lines',
        type: 'line',
        filter: ['match', ['get', 'travelType'], 'WALK', true, false],
        layout: {
          'line-join': 'round',
          'line-cap': 'round'
        },
        paint: {
          'line-color': '#011883',
          'line-width': 3,
          'line-dasharray': [0.2, 2]
        }
      });

      this.map.addLayer({
        id: 'lines-non-walk',
        source: 'lines',
        type: 'line',
        filter: ['match', ['get', 'travelType'], 'WALK', false, true],
        layout: {
          'line-join': 'round',
          'line-cap': 'round'
        },
        paint: {
          'line-color': '#011883',
          'line-width': 3
        }
      });

      this.map.addLayer({
        id: 'start',
        source: 'start',
        type: 'circle',
        paint: {
          'circle-stroke-width': 2,
          'circle-stroke-color': '#fff',
          'circle-radius': 6,
          'circle-color': '#4CBB17'
        }
      });

      this.map.addLayer({
        id: 'end',
        source: 'end',
        type: 'circle',
        paint: {
          'circle-stroke-width': 2,
          'circle-stroke-color': '#fff',
          'circle-radius': 6,
          'circle-color': '#f00'
        }
      });

      this.MAPREADY.next(true);
    });
  }

  populateSources(start: FeatureCollection<Point>, end: FeatureCollection<Point>, lines?: FeatureCollection<LineString>) {
    return new Promise((resolve) => {
      const interval = setInterval(() => {
        if (start.features.length > 0 && end.features.length > 0) {
          try {
            this.map.getSource('start').setData(start);
            this.map.getSource('end').setData(end);
            this.bounds = bbox(featureCollection<Point>([start.features[0], end.features[0]]));
            if (lines && lines.features.length > 0) {
              this.map.getSource('lines').setData(lines);
            }
          } finally {}
          if (this.bounds){
            this.map.fitBounds(this.bounds, {
              padding: 20
            });
            clearInterval(interval);
            resolve();
          }
        }
      }, 1000);
    });
  }
}
