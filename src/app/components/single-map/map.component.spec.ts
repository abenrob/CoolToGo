import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { C2gSingleMapComponent } from '@app/components/single-map/map.component';

describe('C2gSingleMapComponent', () => {
  let component: C2gSingleMapComponent;
  let fixture: ComponentFixture<C2gSingleMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ C2gSingleMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(C2gSingleMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
