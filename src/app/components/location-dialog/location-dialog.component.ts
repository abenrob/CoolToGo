import { Component, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { GeoService } from '@app/services/geo.service';

@Component({
    selector: 'c2g-location-dialog',
    templateUrl: './location-dialog.component.html',
    styleUrls: ['./location-dialog.component.scss']
})
export class C2gLocationDialogComponent {
    validLocation = false;
    @ViewChild('geolocator', { static: true }) geolocator: ElementRef;

    constructor(
        private geoService: GeoService,
        public dialogRef: MatDialogRef<C2gLocationDialogComponent>
    ) {
        this.dialogRef.disableClose = true;
    }

    async validateLocation() {
        this.validLocation = await this.geoService.inMetro((this.geolocator as any).position.geometry.coordinates);
        if (!this.validLocation) {
            (this.geolocator as any).clearInput();
        } else {
            (this.geolocator as any).flagValidLocation();
            this.dialogRef.close();
        }
    }
}

