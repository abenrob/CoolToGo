import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BanService } from '@app/services/ban.service';
import { Feature, Geometry } from '@turf/turf';
import { MatAutocompleteSelectedEvent, MatSnackBar } from '@angular/material';
import { DataService } from '@app/services/data.service';
import { Observable } from 'rxjs';
import { FullUserProfile } from '@app/services/data.service.sources';
import { GeoService } from '@app/services/geo.service';

@Component({
    selector: 'c2g-geolocate',
    templateUrl: './geolocate.component.html',
    styleUrls: ['./geolocate.component.scss']
})
export class C2gGeolocateComponent implements OnInit  {
    geoLocationEnabled = true;
    isSearchingForCurrentLocation: boolean;
    addressOptions: Feature<Geometry>[] = [];
    user$: Observable<FullUserProfile>;
    address: string;
    currentLocationTimeout: any;
    public position: Feature<Geometry>;
    public locationSet = false;
    public addressControl = new FormControl();
    public validLocation = false;

    constructor(
        private banService: BanService,
        private dataService: DataService,
        private geoService: GeoService,
        private snackBar: MatSnackBar
    ) { }

    ngOnInit() {
        this.user$ = this.dataService.fullUser;
        this.user$.subscribe(user => {
            if (user.location && user.location.properties.validated) {
                this.validLocation = true;
                this.addressControl.setValue(user.location);
                this.position = user.location;
                this.locationSet = true;
            }
        });
    }

    getCurrentLocation() {
        this.isSearchingForCurrentLocation = true;
        this.currentLocationTimeout = setTimeout(() => {
            this.getCurrentPositionFailed('Timeout Error');
        }, 4000);

        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition(this.getCurrentPosition.bind(this), this.getCurrentPositionFailed.bind(this));
        } else {
            this.getCurrentPositionFailed('geolocation is not available');
        }
    }

    getCurrentPositionFailed(err: any) {
        console.log(err);
        this.isSearchingForCurrentLocation = false;
        clearTimeout(this.currentLocationTimeout);
    }

    async getCurrentPosition(position: Position) {
        clearTimeout(this.currentLocationTimeout);
        this.isSearchingForCurrentLocation = false;
        this.addressControl.setValue(null);
        this.validLocation = false;
        this.locationSet = false;
        this.addressOptions = [];

        const locations: Feature<Geometry>[] = await this.banService.reverse(
            position.coords.longitude, position.coords.latitude
        ).toPromise();
        if (locations.length > 0) {
            this.geoService.inMetro((locations[0] as any).geometry.coordinates).then(valid => {
                if (valid) {
                    this.position = locations[0];
                    this.locationSet = true;
                    this.flagValidLocation();
                    this.addressControl.setValue(locations[0]);
                } else {
                    this.snackBar.open('La position doit être dans la métropôle de Grenoble', '', {
                        duration: 3000, verticalPosition: 'top'
                    });
                    this.clearInput();
                }
            }).catch(err => {
                console.log(err);
            });
        } else {
            this.snackBar.open('Aucune position trouvé - la position doit être dans la métropôle de Grenoble', '', {
                duration: 3000, verticalPosition: 'top'
            });
            this.clearInput();
        }
    }

    async searchAddresses() {
        this.addressOptions = await this.banService.search(this.addressControl.value).toPromise();
    }

    async onKey(event: KeyboardEvent) {
        if (
            this.addressControl.value &&
            this.addressControl.value.length > 3 &&
            (event.key !== 'ArrowUp' && event.key !== 'ArrowDown')
        ) {
            this.validLocation = false;
            this.searchAddresses();
        }
    }

    validateLocation() {
        this.geoService.inMetro((this.position as any).geometry.coordinates).then(valid => {
            if (valid) {
                this.locationSet = true;
                this.flagValidLocation();
            } else {
                this.snackBar.open('La position doit être dans la métropôle de Grenoble', '', {
                    duration: 3000, verticalPosition: 'top'
                });
                this.clearInput();
            }
        }).catch(err => {
            console.log(err);
        });
    }

    addressDisplay(option?: Feature<Geometry>): string | undefined {
        return option ? option.properties.label : undefined;
    }

    onSelectionChanged(event: MatAutocompleteSelectedEvent) {
        this.validLocation = false;
        this.geoService.inMetro(event.option.value.geometry.coordinates).then(valid => {
            if (valid) {
                this.position = event.option.value;
                this.locationSet = true;
                this.flagValidLocation();
            } else {
                this.snackBar.open('La position doit être dans la métropôle de Grenoble', '', {
                    duration: 3000, verticalPosition: 'top'
                });
                this.clearInput();
            }
        }).catch(err => {
            console.log(err);
        });
    }

    flagValidLocation() {
        this.validLocation = true;
        this.position.properties.validated = true;
        this.dataService.updateUser({
            location: this.position
        });
    }

    clearInput() {
        this.addressControl.setValue(null);
        this.position = null;
        this.locationSet = false;
        this.addressOptions = [];
        this.snackBar.open('La position doit être dans la métropôle de Grenoble', '', {
            duration: 3000, verticalPosition: 'top'
        });
    }
}
