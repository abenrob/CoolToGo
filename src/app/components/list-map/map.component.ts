import { Component, OnInit } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import { FeatureCollection, Point, bbox, MultiPolygon } from '@turf/turf';
import { Observable, BehaviorSubject } from 'rxjs';
import { FullUserProfile } from '@app/services/data.service.sources';
import { APP_DI_CONFIG } from '@app/app-config.module';
import { MatIconRegistry } from '@angular/material';
import { DataService } from '@app/services/data.service';

@Component({
  selector: 'c2g-list-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class C2gListMapComponent implements OnInit {

  /// default settings
  map: mapboxgl.Map;
  style = `https://api.maptiler.com/maps/topo/style.json?key=${APP_DI_CONFIG.maptilerKey}`;
  lat = 45.1885;
  lng = 5.7245;
  mapReady: Observable<boolean>;
  private MAPREADY: BehaviorSubject<boolean>;


  constructor(
    private dataService: DataService,
    private matIconRegistry: MatIconRegistry,
  ) {
    this.MAPREADY = new BehaviorSubject(false) as BehaviorSubject<boolean>;
    this.mapReady = this.MAPREADY.asObservable();
  }

  ngOnInit() {
    this.buildMap();
  }

  buildMap() {
    this.map = new mapboxgl.Map({
      container: 'list-map',
      style: this.style,
      zoom: 14,
      center: [this.lng, this.lat],
      attributionControl: false
    }).addControl(new mapboxgl.NavigationControl()).addControl(new mapboxgl.AttributionControl({
      customAttribution: `<a href='https://targomo.com/developers/resources/attribution/' target='_blank'>&copy; Targomo</a>`,
      compact: false
    }));

    this.map.on('load', async () => {
      this.map.resize();
      await this.map.loadImage('assets/images/pin.png', (error, image) => {
        if (error) { throw error; }
        this.map.addImage('pin', image);
      });

      await this.map.loadImage('assets/images/pin-evt.png', (error, image) => {
        if (error) { throw error; }
        this.map.addImage('pin-evt', image);
      });
      /// register sources
      this.map.addSource('start', {
        type: 'geojson',
        data: {
          type: 'FeatureCollection',
          features: []
        }
      });

      this.map.addSource('locations', {
        type: 'geojson',
        data: {
          type: 'FeatureCollection',
          features: []
        }
      });

      this.map.addSource('polygons', {
        type: 'geojson',
        data: {
          type: 'FeatureCollection',
          features: []
        }
      });

      this.map.addLayer({
        id: 'polygons',
        source: 'polygons',
        type: 'fill',
        paint: {
          'fill-color': '#666',
          'fill-opacity': 0.5
        }
      });

      this.map.addLayer({
        id: 'start',
        source: 'start',
        type: 'circle',
        paint: {
          'circle-stroke-width': 2,
          'circle-stroke-color': '#fff',
          'circle-radius': 8,
          'circle-color': '#4CBB17'
        }
      });

      this.map.addLayer({
        id: 'locations',
        source: 'locations',
        type: 'symbol',
        // filter: ['match', ['get', 'lieu_event'], 'Lieu', true, false],
        layout: {
          'icon-image': 'pin',
          'icon-size': 0.5,
          'icon-allow-overlap': true,
          'icon-anchor': 'bottom'
        }
      });

      this.map.addLayer({
        id: 'locations-event',
        source: 'locations',
        type: 'symbol',
        filter: ['match', ['get', 'lieu_event'], 'Evenement', true, false],
        layout: {
          'icon-image': 'pin-evt',
          'icon-size': 0.5,
          'icon-allow-overlap': true,
          'icon-anchor': 'bottom'
        }
      });

      this.map.on('click', (e) => {
        const f = this.map.queryRenderedFeatures(e.point, { layers: ['locations', 'locations-event'] });
        if (f.length) {
          this.setPopup(f[0]);
        }
      });

      // cursor settings
      this.map.on('mouseenter', 'locations', () => {
        this.map.getCanvas().style.cursor = 'pointer';
      });
      this.map.on('mouseleave', 'locations', () => {
        this.map.getCanvas().style.cursor = '';
      });
      this.map.on('mouseenter', 'locations-event', () => {
        this.map.getCanvas().style.cursor = 'pointer';
      });
      this.map.on('mouseleave', 'locations-event', () => {
        this.map.getCanvas().style.cursor = '';
      });

      this.MAPREADY.next(true);

    });
  }

  async setPopup(e) {
    const coordinates = e.geometry.coordinates.slice();
    const props = e.properties;

    const categoriesSource = this.dataService.getCategories(props.categories);
    const categories = async () => {
        return Promise.all(categoriesSource.map(cat => {
          return new Promise((resolve) => {
            if (cat.icon_type === 'svg') {
              return this.matIconRegistry.getNamedSvgIcon(`${cat.icon}-light`).toPromise().then(svg => {
                resolve(`<i class="mat-icon mat-icon-no-color popup-icon">${svg.outerHTML}</i>`);
              })
            } else {
              resolve(`<i class="material-icons popup-icon">${cat.icon}</i>`)
            }
          })
        }))
    }

    const html = `<div class="popup-content mat-card ${props.lieu_event === 'event' ? 'event' : ''}">
      <div class="popup-header mat-card-header mat-primary">
        <div class="title">${props.title}</div>
        <div class="subtitle mat-body">
          <span><i class="material-icons popup-icon">${props.transportIcon}</i>${Math.round((props.travelTime / 60) * 10) / 10} min</span>
          <span class="categories">${(await categories()).join('')}</span>
        </div>
      </div>
      <div class="popup-body">
        ${props.description_short}
      </div>
      <div class="actions">
        <a href="lieux/${props.id}" class="mat-flat-button mat-primary">voir détails</a>
      </div
    </div>`;

    new mapboxgl.Popup({ offset: [0, -17], maxWidth: '300px' })
      .setLngLat(coordinates)
      .setHTML(html)
      .addTo(this.map);
  }

  populateSources(locations: FeatureCollection<Point>, user: FullUserProfile, polygons: FeatureCollection<MultiPolygon>) {
    return new Promise((resolve) => {
      const interval = setInterval(() => {
        try {
          this.map.getSource('locations').setData(locations);
          this.map.getSource('start').setData(user.location);
          this.map.getSource('polygons').setData(polygons);
          const bounds = bbox(locations);
          this.map.fitBounds(bounds, {
            padding: 30
          });
          clearInterval(interval);
          resolve();
        } finally {}
      }, 1000);
    });
  }
}
