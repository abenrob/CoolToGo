import { Component, Input, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'c2g-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class C2gNavbarComponent implements OnInit {
  @Input() actions: string;
  public innerWidth: any;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.innerWidth = window.innerWidth;
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.innerWidth = window.innerWidth;
  }

  layoutAlign() {
    return this.actions === 'true' && this.innerWidth >= 600 ? 'space-between center' : 'center center';
  }

  goHome() {
    this.router.navigate(['/']);
  }
}
