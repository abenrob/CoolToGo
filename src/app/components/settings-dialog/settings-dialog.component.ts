import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { DataService } from '@app/services/data.service';
import { FullUserProfile, ProfileItem } from '@app/services/data.service.sources';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
    selector: 'c2g-settings-dialog',
    templateUrl: './settings-dialog.component.html',
    styleUrls: ['./settings-dialog.component.scss']
})
export class C2gSettingsDialogComponent implements OnInit {
    settingsForm: FormGroup;
    profiles: any[];
    modesOfTransport: any[];
    geoLocationEnabled = true;
    isSearchingForCurrentLocation: boolean;
    currentLocationTimeout: any;
    validLocation = false;
    user$: Observable<FullUserProfile>;
    times: any = [];
    filters: any[];
    selectedFilter: any;
    selectedProfiles: any[];

    @ViewChild('geolocator', { static: true }) geolocator: ElementRef;

    constructor(
        public dialogRef: MatDialogRef<C2gSettingsDialogComponent>,
        private dataService: DataService,
        private fb: FormBuilder
    ) {
        this.dialogRef.disableClose = true;
        if (!navigator.geolocation) {
            this.geoLocationEnabled = false;
        }
    }

    ngOnInit() {
        this.user$ = this.dataService.fullUser;
        this.user$.subscribe((user: FullUserProfile) => {
            this.settingsForm = this.fb.group({
                modeControl: [null],
                timeControl: [null]
            });
            this.times = [1800, 3600, 5400].map(n => {
                return {
                    value: n,
                    label: `${n / 60} minutes`,
                    selected: n === user.maxTime
                };
            });
            this.settingsForm.get('timeControl').setValue(user.maxTime);
            const mode = user.transportModes.find(m => m.selected);
            this.settingsForm.get('modeControl').setValue(mode);
            this.filters = user.activityFilters;
            this.selectedFilter = user.activityFilters.find(filter => filter.selected);
            this.profiles = user.userProfiles;
            this.selectedProfiles = user.userProfiles.filter(profile => profile.selected);
        });
    }

    setProfiles(event, selected) {
        if (event && selected.length === 0) {
            this.profiles.forEach(profile => profile.selected = true);
            this.selectedProfiles = this.profiles.map(profile => profile);
        } else {
            this.profiles.forEach(profile => {
                const match = selected.find(option => option.value.value === profile.value);
                profile.selected = match ? true : false;
            });
        }
        this.dataService.setProfileFilters(this.profiles);
    }

    setFilter(filter) {
        this.filters.forEach(f => f.selected = f.value === filter.value);
        this.selectedFilter = filter
        this.dataService.setActivityFilters(this.filters);
    }

    filterCompare(a: any, b: any) {
        return a.value === b.value;
    }

    setMode(mode: ProfileItem) {
        this.dataService.setMode(mode);
    }

    setTime(time: number) {
        this.dataService.setTime(time);
    }

    closeDialog() {
        this.dialogRef.close();
    }
}

