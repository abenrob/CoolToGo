import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterialModule } from '@app/modules';
import { C2gNavbarComponent } from '@app/components/navbar/navbar.component';
import { C2gGeolocateComponent } from '@app/components/geolocate/geolocate.component';
import { C2gLocationDialogComponent } from '@app/components/location-dialog/location-dialog.component';
import { C2gSettingsDialogComponent } from '@app/components/settings-dialog/settings-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { C2gSingleMapComponent } from '@app/components/single-map/map.component';
import { C2gListMapComponent } from '@app/components/list-map/map.component';
import { InViewComponent } from '@app/components/in-view/in-view.component';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    C2gNavbarComponent,
    C2gGeolocateComponent,
    C2gLocationDialogComponent,
    C2gSettingsDialogComponent,
    C2gSingleMapComponent,
    C2gListMapComponent,
    InViewComponent
  ],
  exports: [
    C2gNavbarComponent,
    C2gGeolocateComponent,
    C2gLocationDialogComponent,
    C2gSettingsDialogComponent,
    C2gSingleMapComponent,
    C2gListMapComponent,
    InViewComponent
  ]
})
export class ComponentsModule {}
