import { Component } from '@angular/core';

@Component({
  selector: 'c2g-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent {

}
