import { writeFileSync } from 'fs';
import { resolve } from 'path';
const root = resolve(__dirname, '../assets/json');
const categories = require(resolve(root, 'categories.json'));
// const features = require(resolve(root, 'locations_apidae.json')).data.features;
import { greLocations, locationsByCategory } from './villeDeGrenoble';
import { Feature, Point } from '@turf/turf';
import fetch, { RequestInit } from 'node-fetch';


function data(categories: number[], features: Feature<Point>[]): any {
    return {
        "status": 200,
        "locations_returned": features.length,
        "query": {
            "categories": [...categories],
            "profiles": [1, 2, 3, 4, 5, 6]
        },
        "data": {
            "type": "FeatureCollection",
            "name": "cool2go",
            "features": features
        }
    };
}


const reqInit: RequestInit = { 
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    redirect: 'follow', // manual, *follow, error
    body: JSON.stringify({
        "categories" : [1,2,3,4,5,6,7],
        "profiles" : [1,2,3,4,5,6]
    })
}

async function processData() {
    const LocResponse = await fetch(
        "https://cooltogo-staging.herokuapp.com/api/locations", 
        reqInit
    );
    const features = (await LocResponse.json()).data.features
    writeFileSync(resolve(root, `locations.json`), JSON.stringify(data(categories.map(c => c.value), [...features,...greLocations]), null, 4), 'utf8');

    for (let category of categories.map(c => c.value)) {
        const greByCat = locationsByCategory(category);
        const filteredLocations = features.filter(f => f.properties.categories.includes(category));
        writeFileSync(resolve(root, `locations-${category}.json`), JSON.stringify(data([category], [...filteredLocations,...greByCat]), null, 4), 'utf8');
    }
}

processData();