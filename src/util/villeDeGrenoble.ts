import { readFileSync } from 'fs';
import * as parse from 'csv-parse/lib/sync';
import { resolve } from 'path';
const root = resolve(__dirname, '../assets/json');

const profilesLookup = {
    "senior": 1,
    "enfant": 2,
    "jeune": 3,
    "adulte": 4,
    "solidaire": 5
};

export const greLocations = parse(
    readFileSync(resolve(root, 'locations_ville_de_grenoble.csv')),
    { columns: true, skip_empty_lines: true }
).filter(r => r.exclude !== 'TRUE').map(row => {
    let feature: any = {
        type: "Feature",
        properties: {},
        geometry: {
            type: "Point",
            coordinates: [+row.longitude, +row.latitude]
        }
    }

    feature.properties.id = +('38000' + row.id);
    feature.properties.type = "VILLE DE GRENOBLE";
    feature.properties.title = row.name;
    feature.properties.address = row.adresse_1;
    feature.properties.address_2 = row.adresse_2;
    feature.properties.code_postal = row.code_postal;
    feature.properties.city = row.ville;
    feature.properties.tel = null;
    feature.properties.mail = null;
    feature.properties.url = row.info_hours;
    feature.properties.description_short = row.description_teaser;
    feature.properties.description = row.description;
    feature.properties.images = [row.images];
    feature.properties.public = null;
    feature.properties.accessibility = row.accessibility === 'TRUE';
    feature.properties.paying = row.paid === 'TRUE';
    feature.properties.environment = null;
    feature.properties.opening = null;
    feature.properties.date_start = null;
    feature.properties.date_end = null;
    feature.properties.profiles = row.publics.split(',').map(p => profilesLookup[p]);
    feature.properties.categories = row.category.split(';').map(c => +c);
    feature.properties.latitude = +row.latitude;
    feature.properties.longitude = +row.longitude;
    Object.keys(feature.properties)
        .forEach(k => feature.properties[k] = feature.properties[k] === '' ? null : feature.properties[k]);
    return feature;
});

export const locationsByCategory =  function(category){
    return greLocations.filter(loc => loc.properties.categories.includes(category));
}