#!/bin/bash

cp ./src/environments/environment.ts.txt ./src/environments/environment.ts
sed -i "s/TGM_KEY/$TGM_KEY/g" ./src/environments/environment.ts
sed -i "s/MAPTILER_KEY/$MAPTILER_KEY/g" ./src/environments/environment.ts 
sed -i "s/PROD_DEPLOY/$PROD_DEPLOY/g" ./src/environments/environment.ts

